const path = require("path");

if (process.env.CONFIG_FILE_LOCATION != null) {
    module.exports = require(process.env.CONFIG_FILE_LOCATION);
} else {
    module.exports = {
        sendGridApiKey: process.env.SEND_GRID_API_KEY,
        sendGridSendApiEndpoint: process.env.SEND_GRID_SEND_API_ENDPOINT || "https://api.sendgrid.com/v3/mail/send",
        amqpConnectionString: process.env.AMQP_CONNECTION_STRING || "amqp://localhost",
        amqpQueueName: process.env.AMQP_QUEUE_NAME || "email-queue",
        loggingEnableConsoleLogger: JSON.parse(process.env.LOGGING_ENABLE_CONSOLE_LOGGER || "true"),
        loggingConsoleLoggerLevel: process.env.LOGGING_JSON_LOGGER_LEVEL || "info",
        loggingEnableRawLogger: JSON.parse(process.env.LOGGING_ENABLE_RAW_LOGGER || "true"),
        loggingRawLoggerLevel: process.env.LOGGING_RAW_LOGGER_LEVEL || "info",
        loggingRawLoggerDirectory: process.env.LOGGING_RAW_LOGGER_DIRECTORY || "log",
        loggingRawLoggerBaseFileName: process.env.LOGGING_RAW_LOGGER_BASE_FILE_NAME || "app.log",
        loggingRawLoggerFileDatePattern: process.env.LOGGING_RAW_LOGGER_FILE_DATE_PATTERN || "yyyy-MM-dd.",
        loggingEnableJsonLogger: JSON.parse(process.env.LOGGING_ENABLE_JSON_LOGGER || "true"),
        loggingJsonLoggerLevel: process.env.LOGGING_JSON_LOGGER_LEVEL || "info",
        loggingJsonLoggerDirectory: process.env.LOGGING_JSON_LOGGER_DIRECTORY || "log",
        loggingJsonLoggerFileDatePattern: process.env.LOGGING_JSON_LOGGER_FILE_DATE_PATTERN || "yyyy-MM-dd.",
        loggingJsonLoggerBaseFileName: process.env.LOGGING_JSON_LOGGER_BASE_FILE_NAME || "app.log.json"
    }
}