#!/usr/bin/env node

const {promisify} = require("util");
const rp = require("request-promise");
const amqp = require("amqplib");

const config = require("./config");
const logger = require("./logger");

async function main() {
    const connection = await amqp.connect(config.amqpConnectionString);
    const channel = await connection.createChannel();
    
    channel.assertQueue(config.amqpQueueName, {durable: true});

    channel.consume(config.amqpQueueName, async message => {
        try {
            logger.info("Picked up message");
            const messageObject = JSON.parse(message.content.toString());
            logger.debug(messageObject);

            const emailFromAddress = messageObject.fromAddress;
            const emailFromName = messageObject.fromName;

            const subject = messageObject.subject;
            const textContent = messageObject.textContent;
            const htmlContent = messageObject.htmlContent;

            const recipients = messageObject.recipients.map(x => ({email: x.email, name: x.name}));
            const content = [
                { type: "text/plain", value: textContent },
                ...(htmlContent != null ? [{ type: "text/html", value: htmlContent }] : [])
            ];

            const res = await rp({
                uri: config.sendGridSendApiEndpoint,
                method: "POST",
                headers: {
                    "Authorization": `Bearer ${config.sendGridApiKey}`
                },
                body: {
                    personalizations: [
                        {
                            to: recipients
                        }
                    ],
                    from: {
                        email: emailFromAddress,
                        name: emailFromName
                    },
                    subject: subject,
                    content: content
                },
                json: true
            });
            logger.info("Completed message successfully");
        } catch (e) {
            logger.error(e);
        }
    }, {noAck: true})

}


main()
    .then(() => logger.info(`Process started; waiting for messages in ${config.amqpQueueName}`), e => logger.error(e));
