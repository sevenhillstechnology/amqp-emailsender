const winston = require("winston");
const path = require("path");
require("winston-daily-rotate-file");

const config = require("./config");

const winstonTransports = [];

if (config.loggingEnableConsoleLogger) {
    winstonTransports.push(new winston.transports.Console({
        prepend: true,
        level: config.loggingConsoleLoggerLevel,
        humanReadableUnhandledException: true,
        handleExceptions: true,
        json: false,
        colorize: true,
        timestamp: true
    }));
}

if (config.loggingEnableRawLogger) {
    winstonTransports.push(new winston.transports.DailyRotateFile({
        name: "raw",
        filename: path.resolve(__dirname, config.loggingRawLoggerDirectory, config.loggingRawLoggerBaseFileName),
        datePattern: config.loggingRawLoggerFileDatePattern,
        prepend: true,
        level: config.loggingRawLoggerLevel,
        humanReadableUnhandledException: true,
        handleExceptions: true,
        json: false,
        timestamp: true
    }));
}

if (config.loggingEnableJsonLogger) {
    winstonTransports.push(new winston.transports.DailyRotateFile({
        name: "json",
        filename: path.resolve(__dirname, config.loggingJsonLoggerDirectory, config.loggingJsonLoggerBaseFileName),
        datePattern: config.loggingJsonLoggerFileDatePattern,
        prepend: true,
        level: config.loggingJsonLoggerLevel,
        humanReadableUnhandledException: true,
        handleExceptions: true,
        json: false,
        timestamp: true
    }));
}

const logger = new winston.Logger({
    transports: winstonTransports
});

module.exports = logger;